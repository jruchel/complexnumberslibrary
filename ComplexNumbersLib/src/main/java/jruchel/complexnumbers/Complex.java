package jruchel.complexnumbers;


import java.util.Objects;

public class Complex {

    private double real;
    private Imaginary imaginary;

    public Complex(double real) {
        this(real, new Imaginary(0));
    }

    public Complex(double real, Imaginary imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    public Complex(double real, double imaginary) {
        this(real, new Imaginary(imaginary));
    }

    public Imaginary getImaginaryPart() {
        return imaginary;
    }

    public double getRealPart() {
        return real;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        if (getRealPart() != 0) {
            stringBuilder
                    .append(getRealPart());
            if (getImaginaryPart().getValue() == 0) {
                return stringBuilder.toString();
            }
            if (getImaginaryPart().getValue() >= 0) {
                stringBuilder.append("+");
            }
        }

        stringBuilder
                .append(getImaginaryPart());

        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Complex that = (Complex) o;
        return this.getRealPart() == that.getRealPart() && this.getImaginaryPart() == that.getImaginaryPart();
    }

    @Override
    public int hashCode() {
        return Objects.hash(real, imaginary);
    }
}
