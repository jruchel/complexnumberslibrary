package jruchel.complexnumbers;

import java.util.Objects;

public class Imaginary {

    private double value;

    public Imaginary(double value) {
        this.value = value;
    }

    public Imaginary add(Imaginary i) {
        return new Imaginary(this.value + i.value);
    }

    public Imaginary subtract(Imaginary i) {
        return new Imaginary(this.value - i.value);
    }

    public double multiply(Imaginary i) {
        return this.value * i.value * -1d;
    }

    public double divide(Imaginary i) {
        return this.value / i.value * -1d;
    }

    public double getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Imaginary imaginary = (Imaginary) o;
        return Double.compare(imaginary.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return this.value + "i";
    }
}
