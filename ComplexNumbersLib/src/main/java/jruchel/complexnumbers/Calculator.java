package jruchel.complexnumbers;

public class Calculator {

    public static Complex sum(Complex n1, Complex n2) {
        return new Complex(n1.getRealPart() + n2.getRealPart(), n1.getImaginaryPart().add(n2.getImaginaryPart()));
    }

    public static Complex subtract(Complex n1, Complex n2) {
        return new Complex(n1.getRealPart() - n2.getRealPart(), n1.getImaginaryPart().subtract(n2.getImaginaryPart()));
    }

    public static Complex multiply(Complex n1, Complex n2) {
        return new Complex(
                n1.getRealPart() * n2.getRealPart() + n1.getImaginaryPart().multiply(n2.getImaginaryPart()),
                new Imaginary(n1.getImaginaryPart().getValue() * n2.getRealPart() + n1.getRealPart() * n2.getImaginaryPart().getValue()));
    }

    public static Complex divide(Complex n1, Complex n2) {

        Complex numerator = n1;
        double nominator = 0;
        Complex extension = new Complex(n2.getRealPart(), n2.getImaginaryPart().getValue() * -1);

        numerator = multiply(numerator, extension);
        nominator = multiply(n2, extension).getRealPart();

        return new Complex(numerator.getRealPart() / nominator, numerator.getImaginaryPart().getValue() / nominator);
    }

    public static Complex power(Complex n, int pow) {
        Complex result = n;
        for(int i = 1; i < pow; i++) {
            result = multiply(result, n);
        }
        return result;
    }

    public static double modulus(Complex n) {
        double realPart = n.getRealPart() * n.getRealPart();
        double imaginaryPart = n.getImaginaryPart().getValue() * n.getImaginaryPart().getValue();
        double temp = realPart + imaginaryPart;

        return Math.sqrt(temp);
    }

    public static double mainArgument(Complex n) {
        double modulus = modulus(n);
        double sin = n.getImaginaryPart().getValue() / modulus;
        double cos = n.getRealPart() / modulus;
        return Math.atan2(sin, cos);
    }

    public static RoundedValue getRoundedValueOf(double value) {
        return RoundedValue.findRoundedValue(value);
    }

    private enum RoundedValue {
        SQRT_2, SQRT_3, PI, NOT_FOUND;

        public static RoundedValue findRoundedValue(double value) {
            double localVal = Math.ceil(SQRT_2.getValue());
            double givenVal = Math.ceil(value);
            if (localVal == givenVal) {
                return SQRT_2;
            }
            localVal = Math.ceil(SQRT_3.getValue());
            if (localVal == givenVal) {
                return SQRT_3;
            }
            localVal = Math.ceil(PI.getValue());
            if (localVal == givenVal) {
                return PI;
            } else {
                return NOT_FOUND;
            }
        }

        public double getValue() {
            switch (this) {
                case PI:
                    return Math.PI;
                case SQRT_2:
                    return Math.sqrt(2);
                case SQRT_3:
                    return Math.sqrt(3);
                default:
                    return 0;
            }
        }

        public String toString() {
            switch (this) {
                case PI:
                    return "PI";
                case SQRT_2:
                    return "Square root of 2";
                case SQRT_3:
                    return "Square root of 3";
                default:
                    return "Not found";
            }
        }
    }
}

